package com.studios.bobbycarpirat.wgapplication;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.runner.AndroidJUnitRunner;

import com.studios.bobbycarpirat.wgapplication.ui.helperClasses.IdManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class UniqueIDTest {

    @Test
    public void uniqueIDNotNullTest() {
        assertNotNull(IdManager.getUniqueIdWithoutChecking());
    }

    @Test
    public void twentyFollowingIDsNotEqualTest(){
        String[] testIDs = new String[20];

        for (int i = 0; i<20; i++){
            testIDs[i] = IdManager.getUniqueIdWithoutChecking();
        }

        for (int i = 0; i<20; i++){
            for (int j = i+1; j<20; j++){
                assertNotEquals(testIDs[i],testIDs[j]);
            }
        }
    }
}
