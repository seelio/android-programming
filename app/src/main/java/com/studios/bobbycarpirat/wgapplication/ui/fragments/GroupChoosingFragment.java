package com.studios.bobbycarpirat.wgapplication.ui.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.studios.bobbycarpirat.wgapplication.R;
import com.studios.bobbycarpirat.wgapplication.model.WG;
import com.studios.bobbycarpirat.wgapplication.ui.adapter.WGRecyclerAdapter;
import com.studios.bobbycarpirat.wgapplication.ui.helperClasses.IdManager;
import com.studios.bobbycarpirat.wgapplication.ui.helperClasses.SharedPreferencesKeys;
import com.studios.bobbycarpirat.wgapplication.ui.listener.HideButtonOnScrollListener;
import com.studios.bobbycarpirat.wgapplication.ui.listener.RecyclerItemTouchHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GroupChoosingFragment extends Fragment {

    private List<WG> wgList;

    RecyclerView wgRecyclerView;

    final DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    final DatabaseReference mTestRef = mRootRef.child("WG_Eintrage");

    boolean initList;

    final ValueEventListener testEntryListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (initList) {
                initList(dataSnapshot);
                initList = false;
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initList = true;
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_group_choosing, container, false);

        FloatingActionButton newWGButton = view.findViewById(R.id.new_wg_button);
        newWGButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(getString(R.string.new_wg_dialog_header));

                builder.setView(R.layout.new_wg_alert);

                // Set up the buttons
                builder.setPositiveButton(getString(R.string.accept_button_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText name_wg = ((AlertDialog) dialog).findViewById(R.id.editTextName);
                        EditText location_wg = ((AlertDialog) dialog).findViewById(R.id.editTextLocation);
                        createWGEntryInFirebase(name_wg.getText().toString(),
                                location_wg.getText().toString());
                    }
                });
                builder.setNegativeButton(getString(R.string.cancel_button_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

        // TODO: Replace this (Cursor Adapter for database stuff, just example
        wgRecyclerView = view.findViewById(R.id.wg_recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        wgRecyclerView.setLayoutManager(mLayoutManager);
        wgRecyclerView.addOnScrollListener(new HideButtonOnScrollListener(newWGButton));
        wgRecyclerView.setItemAnimator(new DefaultItemAnimator());
        wgRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));

        mTestRef.addValueEventListener(testEntryListener);

        return view;
    }

    public void initList(DataSnapshot dataSnapshot) {

        wgList = new ArrayList<>();

        for(DataSnapshot snapshot : dataSnapshot.getChildren()) {

            HashMap<String, String> test = (HashMap<String, String>) snapshot.getValue();
            WG wg = new WG(test.get("Name")); // TODO: Best Practice hier evtl konstanten zu verwenden (für die Strings?)
            wg.setOrt(test.get("Ort"));
            wg.setId(snapshot.getKey());

            wgList.add(wg);

        }

        SharedPreferences preferences = this.getActivity().getSharedPreferences(SharedPreferencesKeys.WG_INFO, Context.MODE_PRIVATE);

        WGRecyclerAdapter wgAdapter = new WGRecyclerAdapter(wgList);
        wgAdapter.setPreferences(preferences);

        wgRecyclerView.setAdapter(wgAdapter);

        // Hinzufügen des "per Swipe-Lösch"-Adapters
        ItemTouchHelper.Callback callback = new RecyclerItemTouchHelper(wgAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(wgRecyclerView);
    }

    private void createWGEntryInFirebase(String wgName, String wgLocation) {
        DatabaseReference newEntryRef = mTestRef.child(IdManager.getUniqueId());
        newEntryRef.child("Name").setValue(wgName);
        newEntryRef.child("Ort").setValue(wgLocation);
    }
}
