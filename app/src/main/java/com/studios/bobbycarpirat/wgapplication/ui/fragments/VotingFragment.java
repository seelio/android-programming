package com.studios.bobbycarpirat.wgapplication.ui.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.studios.bobbycarpirat.wgapplication.R;
import com.studios.bobbycarpirat.wgapplication.model.Voting;
import com.studios.bobbycarpirat.wgapplication.ui.adapter.VotingRecyclerAdapter;
import com.studios.bobbycarpirat.wgapplication.ui.helperClasses.Decision;
import com.studios.bobbycarpirat.wgapplication.ui.helperClasses.IdManager;
import com.studios.bobbycarpirat.wgapplication.ui.helperClasses.SharedPreferencesKeys;
import com.studios.bobbycarpirat.wgapplication.ui.listener.HideButtonOnScrollListener;
import com.studios.bobbycarpirat.wgapplication.ui.listener.RecyclerItemTouchHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class VotingFragment extends Fragment {

    private List<Voting> votingList;

    RecyclerView votingRecyclerView;

    final DatabaseReference firebaseRefRoot   = FirebaseDatabase.getInstance().getReference();
    final DatabaseReference firebaseRefVoting = firebaseRefRoot.child("Testeintraege");
    final DatabaseReference firebaseRefUser   = firebaseRefRoot.child("Benutzer");
    final DatabaseReference firebaseRefVotes  = firebaseRefRoot.child("Stimme");

    // Damit die Liste mit Abstimmungen nicht jedes Mal neu erstellt wird, wenn eine Abstimmung
    // hinzugefügt wird, wird diese boolean erstellt. Dieser sorgt dafür, dass die Liste nur
    // dann nur angelegt wird, wenn die View erstellt wird (onCreate). TODO: initList wird nur gesetzt, nie ausgelesen
    boolean initList;
    HashMap<String, String> userID_to_user;
    DataSnapshot userDataSnapshot;
    DataSnapshot votingDataSnapshot;
    DataSnapshot voteDataSnapshot;
    boolean userDataRead;
    boolean votingDataRead;
    boolean voteDataRead;
    String current_user_id;

    HashMap<String, String[]> vote_to_votingID_votingResult;

    final ValueEventListener userEntryListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            userDataSnapshot = dataSnapshot;
            userDataRead = true;
            initList();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
        }
    };

    final ValueEventListener testEntryListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            votingDataSnapshot = dataSnapshot;
            votingDataRead = true;
            initList();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
        }
    };

    final ValueEventListener voteEntryListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            voteDataSnapshot = dataSnapshot;
            voteDataRead = true;
            initList();
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    final View.OnClickListener newVotingButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getString(R.string.title_new_voting_alert));

            builder.setView(R.layout.new_voting_alert);

            // Set up the buttons
            builder.setPositiveButton(getString(R.string.accept_button_text), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    EditText editTextTitle =
                            ((AlertDialog) dialog).findViewById(R.id.editTextTitle);
                    EditText editTextDescr =
                            ((AlertDialog) dialog).findViewById(R.id.editTextDescr);
                    CheckBox checkBoxAbstention =
                            ((AlertDialog) dialog).
                                    findViewById(R.id.checkBoxAbstention);
                    String abstentionEnabled = "False";
                    if (checkBoxAbstention.isChecked()) {
                        abstentionEnabled = "True";
                    }
                    createVotingEntryInFirebase(editTextTitle.getText().toString(),
                            editTextDescr.getText().toString(), abstentionEnabled);
                }
            });
            builder.setNegativeButton(getString(R.string.cancel_button_text), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        }
    };


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        initList = true;
        userDataRead = false;
        votingDataRead = false;
        View view = inflater.inflate(R.layout.fragment_voting, container, false);
        votingRecyclerView = view.findViewById(R.id.voting_recycler_view);

        final FloatingActionButton newVotingButton =
                view.findViewById(R.id.new_voting_button);
        newVotingButton.setOnClickListener(newVotingButtonListener);
        votingRecyclerView.addOnScrollListener(new HideButtonOnScrollListener(newVotingButton));

        firebaseRefUser.addListenerForSingleValueEvent(userEntryListener);
        firebaseRefVoting.addValueEventListener(testEntryListener);
        firebaseRefVotes.addListenerForSingleValueEvent(voteEntryListener);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getContext());
        votingRecyclerView.setLayoutManager(mLayoutManager);
        votingRecyclerView.setItemAnimator(new DefaultItemAnimator());
        votingRecyclerView.addItemDecoration(new DividerItemDecoration(this.getContext(),
                LinearLayoutManager.VERTICAL));

        return view;
    }

    public void initList() {
        if (userDataRead && votingDataRead && voteDataRead) {

            SharedPreferences user_preferences = getActivity().getSharedPreferences(SharedPreferencesKeys.USER_INFO, Context.MODE_PRIVATE);
            current_user_id = user_preferences.getString(SharedPreferencesKeys.USER_ID, "Leer");

            vote_to_votingID_votingResult = new HashMap<>();

            initUserData();

            votingList = new ArrayList<>();

            SharedPreferences preferences = this.getActivity().getSharedPreferences(SharedPreferencesKeys.WG_INFO,
                    Context.MODE_PRIVATE);
            String wg_id = preferences.getString(SharedPreferencesKeys.WG_ID, "Leer");

            SharedPreferences preferences_user = this.getActivity().
                    getSharedPreferences(SharedPreferencesKeys.USER_INFO, Context.MODE_PRIVATE);

            for(DataSnapshot snapshot : votingDataSnapshot.getChildren()) {

                HashMap<String, String> test = (HashMap<String, String>) snapshot.getValue();

                String wg_id_child = test.get("WG_ID");

                if (wg_id.equals(wg_id_child)) {
                    Voting voting = new Voting(test.get("Titel"));
                    voting.setExtDesc(test.get("Beschreibung"));
                    voting.setInitiator(getUsernameFromId(test.get("Initiator")));
                    voting.setId(snapshot.getKey());

                    String result = null;

                    for (DataSnapshot voteSnapshot : voteDataSnapshot.getChildren()) {
                        if (current_user_id.equals(((HashMap<String, String>)
                                voteSnapshot.getValue()).get("Benutzer_ID"))) {
                            if (snapshot.getKey().equals(((HashMap<String, String>)
                                    voteSnapshot.getValue()).get("Umfrage_ID"))) {
                                result = ((HashMap<String, String>)
                                        voteSnapshot.getValue()).get("Antwort");
                            }
                        }
                    }

                    if ("Ja".equals(result)) {
                        voting.setVote(Decision.GREEN);
                    } else if ("Nein".equals(result)) {
                        voting.setVote(Decision.RED);
                    } else if ("Egal".equals(result)) {
                        voting.setVote(Decision.ORANGE);
                    }

                    votingList.add(voting);
                }
            }

            for (DataSnapshot voteSnapshot : voteDataSnapshot.getChildren()) {
                if (current_user_id.equals(((HashMap<String, String>)
                        voteSnapshot.getValue()).get("Benutzer_ID"))) {

                    String[] votingID_votingResult = {voteSnapshot.getKey(), "1"};
                    vote_to_votingID_votingResult.put(((HashMap<String, String>) voteSnapshot.getValue()).
                            get("Umfrage_ID"), votingID_votingResult);
                }
            }


            VotingRecyclerAdapter votingAdapter = new VotingRecyclerAdapter(votingList);
            votingAdapter.setPreferences(preferences_user);
            votingAdapter.setContext(getContext());

            votingRecyclerView.setAdapter(votingAdapter);

            // Hinzufügen des "per Swipe-Lösch"-Adapters
            ItemTouchHelper.Callback callback = new RecyclerItemTouchHelper(votingAdapter);
            ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
            touchHelper.attachToRecyclerView(votingRecyclerView);
        }
    }

    private void initUserData() {
        userID_to_user = new HashMap<>();
        for (DataSnapshot snapshot : userDataSnapshot.getChildren()) {
            HashMap<String, String> test = (HashMap<String, String>) snapshot.getValue();
            userID_to_user.put(snapshot.getKey(), test.get("Benutzername"));
        }
    }

    private String getUsernameFromId(String userId) {
        return userID_to_user.get(userId);
    }

    private void createVotingEntryInFirebase(String voting_title, String voting_descr,
                                             String voting_abstention) {

        SharedPreferences preferences_user = this.getActivity().getSharedPreferences(
                SharedPreferencesKeys.USER_INFO, Context.MODE_PRIVATE);
        String user_id = preferences_user.getString(SharedPreferencesKeys.USER_ID, "Leer");

        SharedPreferences preferences_wg = this.getActivity().getSharedPreferences(SharedPreferencesKeys.WG_INFO,
                Context.MODE_PRIVATE);
        String wg_id = preferences_wg.getString(SharedPreferencesKeys.WG_ID, "Leer");

        DatabaseReference newEntryRef = firebaseRefVoting.child(IdManager.getUniqueId());
        newEntryRef.child("Beschreibung").setValue(voting_descr);
        newEntryRef.child("EnthaltungZulassen").setValue(voting_abstention);
        newEntryRef.child("Initiator").setValue(user_id);
        newEntryRef.child("Titel").setValue(voting_title);
        newEntryRef.child("WG_ID").setValue(wg_id);
    }
}
