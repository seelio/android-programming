package com.studios.bobbycarpirat.wgapplication.ui.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.studios.bobbycarpirat.wgapplication.ui.helperClasses.Decision;
import com.studios.bobbycarpirat.wgapplication.R;
import com.studios.bobbycarpirat.wgapplication.model.Voting;
import com.studios.bobbycarpirat.wgapplication.ui.activity.MainActivity;
import com.studios.bobbycarpirat.wgapplication.ui.helperClasses.IdManager;
import com.studios.bobbycarpirat.wgapplication.ui.helperClasses.SharedPreferencesKeys;
import com.studios.bobbycarpirat.wgapplication.ui.listener.RecyclerItemTouchHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class VotingRecyclerAdapter extends RecyclerView.Adapter<VotingRecyclerAdapter.VotingViewHolder> implements RecyclerItemTouchHelper.ItemTouchHelperAdapter {

    private List<Voting> votingList;

    public VotingRecyclerAdapter(List<Voting> votingList)
    {
        this.votingList=votingList;
    }

    private DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mTestRef = mRootRef.child("Stimme");

    ArrayList<String> votes;

    SharedPreferences preferences;
    Context context;
    String id_current_user;

    @NonNull
    @Override
    public VotingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.voting_list_item, viewGroup, false);

        ValueEventListener userEntryListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                votes = new ArrayList<>();
                id_current_user = preferences.getString(SharedPreferencesKeys.USER_ID, "Leer");
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    HashMap<String, String> values =  (HashMap) snapshot.getValue();
                    String user_id      = values.get("Benutzer_ID");
                    String voting_id    = values.get("Umfrage_ID");
                    if (id_current_user.equals(user_id)) {
                        votes.add(voting_id);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        mTestRef.addListenerForSingleValueEvent(userEntryListener);

        return new VotingViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VotingViewHolder votingViewHolder, int position) {
        Voting voting = votingList.get(position);
        votingViewHolder.votingNameTextView.setText(voting.getShortName());
        votingViewHolder.votingInitiator.setText(voting.getInitiator());
        System.out.println(voting.getVote());
        if (!(voting.getVote() == null)) {
            votingViewHolder.visualSelection(voting.getVote());
        }
    }

    @Override
    public int getItemCount() {
        return votingList.size();
    }

    @Override
    public void removeItem(int position) {
        // TODO: Löschen von Votings
        notifyDataSetChanged();
    }


    public class VotingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        final LinearLayout listItem;
        final TextView votingNameTextView;
        final TextView votingInitiator;
        final ImageView trLightGreen;
        final ImageView trLightOrange;
        final ImageView trLightRed;

        public VotingViewHolder(@NonNull View itemView) {
            super(itemView);
            listItem = itemView.findViewById(R.id.ll_voting_list_item);
            votingNameTextView = itemView.findViewById(R.id.voting_short);
            votingInitiator = itemView.findViewById(R.id.voting_initiator);
            trLightGreen = itemView.findViewById(R.id.tr_light_green);
            trLightOrange = itemView.findViewById(R.id.tr_light_orange);
            trLightRed = itemView.findViewById(R.id.tr_light_red);

            listItem.setOnClickListener(this);
            trLightGreen.setOnClickListener(this);
            trLightOrange.setOnClickListener(this);
            trLightRed.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if(votes.contains(votingList.get(getAdapterPosition()).getId())) {

                if (!(v.getId() == R.id.ll_voting_list_item)) {

                    Toast toast = Toast.makeText(context, "Es wurde bereits abgestimmt", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }
            }

            switch (v.getId()){
                case R.id.ll_voting_list_item:
                    MainActivity activity = (MainActivity) v.getContext();
                    activity.showVoting(votingList.get(getAdapterPosition()).getShortName(),
                            votingList.get(getAdapterPosition()).getExtDesc(),
                            votingList.get(getAdapterPosition()).getId());
                    break;
                case R.id.tr_light_green:
                    addVote(votingList.get(getAdapterPosition()).getId(), "Ja");
                    visualSelection(Decision.GREEN);
                    break;
                case R.id.tr_light_orange:
                    addVote(votingList.get(getAdapterPosition()).getId(), "Egal");
                    visualSelection(Decision.ORANGE);
                    break;
                case R.id.tr_light_red:
                    addVote(votingList.get(getAdapterPosition()).getId(), "Nein");
                    visualSelection(Decision.RED);
                    break;
            }
        }


        private void visualSelection(Decision selected) {
            // TODO: Initiales Laden der Entscheidung bzw. Darstellung einer evtl gespeicherten Entscheidung
            switch (selected) {
                case GREEN:
                    trLightGreen.setColorFilter(listItem.getResources().getColor(R.color.trLightGreen));
                    trLightOrange.setColorFilter(listItem.getResources().getColor(R.color.trLightOrange_deselected));
                    trLightRed.setColorFilter(listItem.getResources().getColor(R.color.trLightRed_deselected));
                    break;
                case ORANGE:
                    trLightGreen.setColorFilter(listItem.getResources().getColor(R.color.trLightGreen_deselected));
                    trLightOrange.setColorFilter(listItem.getResources().getColor(R.color.trLightOrange));
                    trLightRed.setColorFilter(listItem.getResources().getColor(R.color.trLightRed_deselected));
                    break;
                case RED:
                    trLightGreen.setColorFilter(listItem.getResources().getColor(R.color.trLightGreen_deselected));
                    trLightOrange.setColorFilter(listItem.getResources().getColor(R.color.trLightOrange_deselected));
                    trLightRed.setColorFilter(listItem.getResources().getColor(R.color.trLightRed));
                    break;
                default:
                    trLightGreen.setColorFilter(listItem.getResources().getColor(R.color.trLightGreen));
                    trLightOrange.setColorFilter(listItem.getResources().getColor(R.color.trLightOrange));
                    trLightRed.setColorFilter(listItem.getResources().getColor(R.color.trLightRed));
                    break;
            }
        }
    }

    public void setPreferences(SharedPreferences newPreferences) {
        this.preferences = newPreferences;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private void addVote(String idVoting, String vote) {
        if (!votes.contains(idVoting)) {
            DatabaseReference newEntryRef = mTestRef.child(IdManager.getUniqueId());
            newEntryRef.child("Antwort").setValue(vote);
            newEntryRef.child("Benutzer_ID").setValue(id_current_user);
            newEntryRef.child("Umfrage_ID").setValue(idVoting);
            votes.add(idVoting);
        }
    }
}
