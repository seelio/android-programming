package com.studios.bobbycarpirat.wgapplication.model;

public class Todo {
    private String name;
    private Boolean done;
    private Integer customid;

    public Todo(String name, Boolean done, Integer id) {
        this.name = name;
        this.done = done;
        this.customid = id;
    }

    public Todo(String name, Boolean done) {
        this.name = name;
        this.done = done;
    }

    public Todo(String name, String done, String id) {
        this.name = name;
        this.done = done.equals("true");
        this.customid = Integer.valueOf(id);
    }

    public Todo(String name, String done) {
        this.name = name;
        this.done = done.equals("true");
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDone() {
        return done;
    }

    public String getDoneString(){
        if (done)
            return "true";
        else
            return "false";
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public Integer getCustomid() {
        return this.customid;
    }

    public void setCustomid(Integer id) {
        this.customid = id;
    }

    public String getCustomidString(){
        return String.valueOf(this.customid);
    }
}
