package com.studios.bobbycarpirat.wgapplication.model;

import com.studios.bobbycarpirat.wgapplication.ui.helperClasses.Decision;

public class Voting {
    private String shortName;
    private String extDesc;
    private String initiator;

    private String id;

    private Decision vote = null;

    public Voting(String shortName) {
        this.shortName=shortName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getExtDesc() {
        return extDesc;
    }

    public void setExtDesc(String extDesc) {
        this.extDesc = extDesc;
    }

    public String getInitiator() { return initiator; }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public Decision getVote() { return vote; }

    public void setVote(Decision vote)  { this.vote = vote; }
}
