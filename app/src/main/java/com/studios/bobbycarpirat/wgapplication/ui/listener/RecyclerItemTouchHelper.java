package com.studios.bobbycarpirat.wgapplication.ui.listener;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

/**
 * Helfer Klasse, zu nutzen zum Entfernen von Objekten aus RecyclerViews.
 * Definiert das Interface, das von den Adaptern implementiert werden muss sowie die Anbindung an
 * ItemTouchHelper.SimpleCallback.
 * Es wird bei einem Swipe nach Rechts eine "removeItem" Aktion ausgeführt, diese kann in den
 * Adaptern individuell ausgelegt werden.
 */
public class RecyclerItemTouchHelper extends ItemTouchHelper.SimpleCallback {

    private final ItemTouchHelperAdapter recyclerAdapter;

    public RecyclerItemTouchHelper(ItemTouchHelperAdapter recyclerAdapter) {
        super(0, ItemTouchHelper.RIGHT);
        this.recyclerAdapter = recyclerAdapter;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                          RecyclerView.ViewHolder target) {
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        recyclerAdapter.removeItem(viewHolder.getAdapterPosition());
    }

    public interface ItemTouchHelperAdapter {
        void removeItem(int position);
    }
}