package com.studios.bobbycarpirat.wgapplication.ui.listener;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;


/**
 * Dieser Listener sollte für jeden Floating Action Button genutzt werden, der Inhalte verdecken
 * könnte.
 * Wenn der Nutzer nach unten scrollt, wird der Button ausgeblendet, bei einem scrollen nach oben
 * taucht der Button (wieder) auf.
 * Dadurch können sonst unerreichbare Inhalte und Steuerelemente (zB in der Voting Übersicht)
 * erreichbar gemacht werden.
 */
public class HideButtonOnScrollListener extends RecyclerView.OnScrollListener {
    private final FloatingActionButton floatingActionButton;

    public HideButtonOnScrollListener(FloatingActionButton floatingActionButton) {
        super();
        this.floatingActionButton = floatingActionButton;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (dy > 0 && floatingActionButton.isShown())
            floatingActionButton.hide();

        if (dy < 0 && floatingActionButton.isOrWillBeHidden()) {
            floatingActionButton.show();
        }
    }
}

